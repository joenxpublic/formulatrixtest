package id.formulatrix;

import java.util.ArrayList;
import java.util.Iterator;

import org.apache.log4j.Logger;

public class RepositoryServicesImpl implements RepositoryServices {
	final static Logger logger = Logger.getLogger(RepositoryServicesImpl.class);
	private ArrayList<RepositoryBean> repository = null;

	public void register(String itemName, String itemContent, int itemType) {
		try {
			RepositoryBean repositoryBean = new RepositoryBean();
			repositoryBean.setItemName(itemName);
			repositoryBean.setItemContent(itemContent);
			repositoryBean.setItemType(itemType);
			
			repository.add(repositoryBean);
			logger.info("register: ".concat(itemName).concat(" ...Success!!!"));
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	@SuppressWarnings("finally")
	public String retrieve(String itemName) {
		String result = "--------------------------- ".concat(itemName);
		try {
	        Iterator<RepositoryBean> itr = repository.iterator();
	        while (itr.hasNext()) {
	        	RepositoryBean rb = itr.next();
				if(rb.getItemName().equalsIgnoreCase(itemName)) {
					result += "\nname: ".concat(rb.getItemName()).concat("\ntype: ").concat(rb.getItemType()==1?"JSON":"XML").concat("\ncontent: ").concat(rb.getItemContent());
					return result;
				}
	        }
	        
	        result += " - not found";
		} catch (Exception e) {
			logger.error(e.getMessage());
			result += "retrieve error: ".concat(e.getMessage());
		}finally {
			return result;
		}
	}

	@SuppressWarnings("finally")
	public int getType(String itemName) {
		int result = 0;
		try {
	        Iterator<RepositoryBean> itr = repository.iterator();
	        while (itr.hasNext()) {
	        	RepositoryBean rb = itr.next();
				if(rb.getItemName().equalsIgnoreCase(itemName))
					result = rb.getItemType();
	        }
		} catch (Exception e) {
			logger.error(e.getMessage());
		}finally {
			return result;
		}
	}

	public void deregister(String itemName) {
		logger.info("deregister...");
        Iterator<RepositoryBean> itr = repository.iterator();
        while (itr.hasNext()) {
        	RepositoryBean rb = itr.next();
            if (rb.getItemName().equalsIgnoreCase(itemName)) {
                itr.remove();
            }
        }
		logger.info("deregister after: ");
        itr = repository.iterator();
        while (itr.hasNext()) {
        	RepositoryBean rb = itr.next();
			String str = "name: ".concat(rb.getItemName()).concat(", type: ").concat(rb.getItemType()==1?"JSON":"XML").concat(", content: ").concat(rb.getItemContent());
			logger.info(str);
        }
	}

	public void initialize() {
		repository = new ArrayList<RepositoryBean>();
	}

}
