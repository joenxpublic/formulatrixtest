package id.formulatrix;

public class Xtest {

	public static void main(String[] args) {
		RepositoryServicesImpl impl = new RepositoryServicesImpl();
		String itemName = "";
		String itemContent = "";
		int itemType = 1;
		
		impl.initialize();
		
		//add repository
		itemName = "satu";
		itemContent = "{\"Test\": {\"name\": \"name 1\",\"content\": \"content 1\"}}";
		itemType = 1;
		impl.register(itemName, itemContent, itemType);

		//add repository
		itemName = "dua";
		itemContent = "<menu name=\"name 2\" content=\"content 2\"/>";
		itemType = 2;
		impl.register(itemName, itemContent, itemType);

		//add repository
		itemName = "tiga";
		itemContent = "{\"Test\": {\"name\": \"name 3\",\"content\": \"content 3\"}}";
		itemType = 1;
		impl.register(itemName, itemContent, itemType);

		//retrieve
		itemName ="dua";
		System.out.println("retrieve: "+impl.retrieve(itemName));
		itemName ="lima";
		System.out.println("retrieve: "+impl.retrieve(itemName));
	
		//deregister
		itemName ="dua";
		impl.deregister(itemName);
		
	}

}
