package id.formulatrix;

public interface RepositoryServices {
	// Store an item to the repository.
	// Parameter itemType is used to differentiate JSON or XML.
	// 1 = itemContent is a JSON string.
	// 2 = itemContent is an XML string.
	void register(String itemName, String itemContent, int itemType);

	// Retrieve an item from the repository.
	String retrieve(String itemName);

	// Retrieve the type of the item (JSON or XML).
	int getType(String itemName);

	// Remove an item from the repository.
	void deregister(String itemName);

	// Initialize the repository for use, if needed.
	// You could leave it empty if you have your own way to make the repository
	// ready for use
	// (e.g. using the constructor).
	void initialize();

}
